<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::Resource('home/order', 'OrderController')->middleware('auth');
Route::Resource('home/orderItem', 'OrderItemController')->middleware('auth');
Route::Resource('home/product', 'ProductController')->middleware('auth');
Route::Resource('home/category', 'CategoryController')->middleware('auth');
Route::Resource('home/status', 'StatusController')->middleware('auth');

Route::get('home', 'HomeController@index')->name('home');

Route::get('login/{provider}', 'SocialController@redirect');
Route::get('login/{provider}/callback','SocialController@callback');


//Route::view('products', 'products.index',['products'=> 1]);
Route::get('city', 'CityController@index')->name('city');
Route::get('state', 'StateController@index')->name('state');
Route::get('country', 'CountryController@index')->name('country');

Route::resource('order', 'OrderController');

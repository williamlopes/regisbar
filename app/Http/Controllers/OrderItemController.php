<?php

namespace App\Http\Controllers;


use App\OrderItem;
use App\Order;
use Illuminate\Http\Request;

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return OrderItem::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(OrderItem $orderItem)
    {
        return view('orderItem.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
        $validate = $request->validate([
            'quantity'      => 'required',
            'price'         => 'required',
            'discount'      => 'required',
            'paid'          => 'required',
        ]);

        $orderItem = new OrderItem();
        $orderItem->fill($validate);
        $orderItem->save();
        return $orderItem;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        //dd($id);
        $itens = OrderItem::where(['order_id' => $id])->get();
        // dd($itens);
        
        return view('orderItem.edit',compact('itens'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderItem $orderItem)
    {
        //
        dd($orderItem);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderItem $orderItem)
    {
        
        $validate = $request->validate([
            'quantity'      => 'required',
            'price'         => 'required',
            'discount'      => 'required',
            'paid'          => 'required',
        ]);
        
        $orderItem->fill($validate);
        
        $orderItem->save();

        $itens = OrderItem::where(['order_id' => $orderItem->order_id])->get();
        
        
        return view('orderItem.edit',compact('itens'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderItem $orderItem)
    {
        if($orderItem->delete()){
            return $orderItem;
        }
        return false;
    }
}

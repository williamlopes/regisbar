<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use Illuminate\Support\Arr;
use Illuminate\View\View;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function index() : View
    {
        $orders =  Order::with('orderItem.product')->where(['user_id' => Auth::id()])->get();
        return view('order.index', compact('orders'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Order $order) :View
    {
        $products =  Product::with('category')->get();
        // dd($products);
        return view('order.create', compact('products'));
        // return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(User $user,Request $request)
    {
        dump($user);exit;
        $request['user_id'] = Auth::user()->id;
        $validateOrder = $request->validate([
            'order' => 'required',
            'user_id' => $user->id,
            'quantity'         => 'required|array',
            'product_id'   => 'required|array',
            'sendObservation'   => 'max:250',
            'observation'   => 'max:250',
        ]);

        $itens = Arr::pull($request, 'quantity');
        $discount = Arr::pull($request, 'discount');
        $products = Arr::pull($request, 'product_id');


        // $validateOrder['idUser'] = Auth::user()->id;

        $order = new Order();
        $order->fill($validateOrder);
        $order->save();

        // dd($itens);
        // array:3 [
        //     1 => "11"
        //     2 => "33"
        //     3 => "22"
        // ]


        foreach($itens as $product_id => $quantity){
            $orderItem = new OrderItem();

            $item = [
                'order_id'    =>  $order->id,
                'product_id'    => $product_id,
                'quantity'    => $quantity,
                'price'    => $products[$product_id],
                'discount'         => $discount,
            ];
            $orderItems[] = $item;

            $orderItem->fill($item);
            $orderItem->save();


        }

        // return view('order.index');
        $orders =  Order::with('orderItem.product')->where(['user_id' => Auth::id()])->get();
        return view('order.index', compact('orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order  $order
     * @return View
     */
    public function show(Order $order) :view
    {
        $orders =  Order::with('orderItem')->where(['user_id' => Auth::id()])->get();
        return view('order.show', compact('orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $validate = $request->validate([
            'order_id'      => 'required',
            'product_id'    => 'required',
            'quantity'      => 'required|number|min:0',
            'price'         => 'required',
        ]);

        // $order = new order();
        $order->fill($validate);
        $order->save();
        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        if($order->delete()){
            return $order;
        }
        return false;
    }
}

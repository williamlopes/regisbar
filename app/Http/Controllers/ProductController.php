<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products =  Product::with('category')->get();
//        dd($products[0]->category[0]->id);
        return view('products.index', compact('products',$products));
    }


    public function edit(Product $product)
    {
        $product::with('categories');
        $categories = Category::all();
        return view('products.edit', compact(['product','categories']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Product
     */

    public function store(Request $request)
    {
        $validate = $request->validate([
            'name'        => 'required|string|min:5',
            'description'       => 'required|min:20',
            'price'       => 'required|min:3',
            'categories'  => 'required',
        ]);

        $product = new Product();
        $product->fill($validate);
        $product->save();
        $product->category()->sync($validate['categories']);
        $product->status()->sync($validate['status']);
        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  Product  $product
     * @return Product
     */


    public function show(Product $product)
    {
        return $product;
    }


    public function update(Request $request ,Product $product)
    {

        $validate = $request->validate([
            'name'        => 'required|string|min:5',
            'description' => 'required|min:20',
            'price'       => 'required|min:3',
            'active'  => 'required',
            'categories' => 'required' ,

        ]);

        $product->fill($validate);
        $product->save();
        $product->category()->sync($validate['categories']);
//        $product->status()->sync($validate['status']);

//        return $product;
        return redirect('/home/product')->with('success', 'Product updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return Product
     * @throws \Exception
     * @throws \Exception
     */


    public function destroy(Product $product)
    {
        if($product->delete()){
            return redirect('/home/product')->with('success', 'Product deleted!');
        }
        return redirect('/home/status')->with('fail', 'Product not deleted!');

    }
}

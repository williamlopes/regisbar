<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['order','user_id','discount','observation','sendObservation'];

    public function address(){
        return $this->hasOne(Address::class);
    }

    public function payment(){
        return $this->hasMany(Payment::class);
    }

    public function orderItem(){
        return $this->hasMany(OrderItem::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

//    public function OrderItem(){return $this->belongsToMany(OrderItem::class);}
//    public function shipment(){}
//    public function traking(){}
}

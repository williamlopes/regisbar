<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','streetName','number','zipCode','district','complement','observation'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

//    public function state()
//    {
//        return $this->belongsTo(State::class);
//    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function order()
    {
        return $this->belongsToMany(Order::class);
    }

}

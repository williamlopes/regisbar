<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'code','name','description','price', 'netWeight','weight','height','width','depth', 'exclusive', 'gtin', 'factorScore', 'isService', 'bonus' , 'active'
    ];

    public function category()
    {
        return $this->belongsToMany(Category::class);
    }

    public function status()
    {
        return $this->hasMany(Status::class);
    }

    // public function order()
    // {
    //     return $this->belongsTo(Order::class);
    // }

    // public function productItem()
    // {
    //     return $this->belongsToMany(ProductItem::class);
    // }

   public function orderItem()
   {
       return $this->belongsTo(OrderItem::class);
   }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['code','name'] ;
    public $timestamps = null;
    public function state()
    {
        return $this->hasMany(State::class);
    }
}

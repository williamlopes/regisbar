<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert(
        [
            [ 'name' => 'Acre' ,'code' => 12 , 'country_id' => 30 ],
            [ 'name' => 'Alagoas' ,'code' => 27 , 'country_id' => 30 ],
            [ 'name' => 'Amapá' ,'code' => 16 , 'country_id' => 30 ],
            [ 'name' => 'Amazonas' ,'code' => 13 , 'country_id' => 30 ],
            [ 'name' => 'Bahia' ,'code' => 29 , 'country_id' => 30 ],
            [ 'name' => 'Ceará' ,'code' => 23 , 'country_id' => 30 ],
            [ 'name' => 'Distrito Federal' ,'code' => 53 , 'country_id' => 30 ],
            [ 'name' => 'Espírito Santo' ,'code' => 32 , 'country_id' => 30 ],
            [ 'name' => 'Goiás' ,'code' => 52 , 'country_id' => 30 ],
            [ 'name' => 'Maranhão' ,'code' => 21 , 'country_id' => 30 ],
            [ 'name' => 'Mato Grosso' ,'code' => 51 , 'country_id' => 30 ],
            [ 'name' => 'Mato Grosso do Sul' ,'code' => 50 , 'country_id' => 30 ],
            [ 'name' => 'Minas Gerais' ,'code' => 31 , 'country_id' => 30 ],
            [ 'name' => 'Pará' ,'code' => 15 , 'country_id' => 30 ],
            [ 'name' => 'Paraíba' ,'code' => 25 , 'country_id' => 30 ],
            [ 'name' => 'Paraná' ,'code' => 41 , 'country_id' => 30 ],
            [ 'name' => 'Pernambuco' ,'code' => 26 , 'country_id' => 30 ],
            [ 'name' => 'Piauí' ,'code' => 22 , 'country_id' => 30 ],
            [ 'name' => 'Rio Grande do Norte' ,'code' => 24 , 'country_id' => 30 ],
            [ 'name' => 'Rio Grande do Sul' ,'code' => 43 , 'country_id' => 30 ],
            [ 'name' => 'Rio de Janeiro' ,'code' => 33 , 'country_id' => 30 ],
            [ 'name' => 'Rondônia' ,'code' => 11 , 'country_id' => 30 ],
            [ 'name' => 'Roraima' ,'code' => 14 , 'country_id' => 30 ],
            [ 'name' => 'Santa Catarina' ,'code' => 42 , 'country_id' => 30 ],
            [ 'name' => 'São Paulo' ,'code' => 35 , 'country_id' => 30 ],
            [ 'name' => 'Sergipe' ,'code' => 28 , 'country_id' => 30 ],
            [ 'name' => 'Tocantins', 'code' => 17 , 'country_id' => 30],
        ]);
    }
}

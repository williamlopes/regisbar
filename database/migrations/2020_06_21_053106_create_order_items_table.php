<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->unsignedFloat('quantity');
            $table->unsignedFloat('price');
            $table->unsignedFloat('paid')->default(0);
            $table->unsignedFloat('discount')->default(0);
            $table->timestamps();
            $table->foreing('order_id')->refencies('id')->on('order');
            $table->foreing('product_id')->refencies('id')->on('product');
        

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigIncrements('order')->default(null);
            // $table->unsignedInteger('company');
            $table->unsignedFloat('discount')->default(0);
            $table->string('observation')->nullable();
            $table->string('sendObservation')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('address_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('payment_id')->unsigned();
            // $table->bigInteger('shipment_id')->unsigned();
            // $table->bigInteger('tracking_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->referencies('id')->on('user');
            $table->foreign('address_id')->referencies('id')->on('address');
            // $table->foreign('product_id')->referencies('id')->on('product');
            $table->foreign('payment_id')->referencies('id')->on('payment');
            // $table->foreign('shipment_id')->referencies('id')->on('shipment');
            // $table->foreign('tracking_id')->referencies('id')->on('tracking');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->randomFloat(0,0,1000),
        'description' => $faker->sentence,
        'price' => $faker->randomFloat(2,0,10000),
        'netWeight' => $faker->randomFloat(2,0,1000),
        'weight' => $faker->randomFloat(2,0,1000),
        'height'=> $faker->randomFloat(2,0,1000),
        'width' => $faker->randomFloat(2,0,1000),
        'depth' => $faker->randomFloat(2,0,1000),
        'exclusive'=> $faker->boolean,
        'gtin'=> $faker->ean13,
        'factorScore'=> 0,
        'isService'=> $faker->boolean,
        'bonus' => $faker->boolean,
        'active' => $faker->boolean
    ];
});

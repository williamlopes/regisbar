@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h4 class="display-3">Update a Category</h4>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br />
        @endif
        <form method="post" action="{{ route('category.update', $category->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{$category->name}}" />

            </div>
            <div class="form-group">
                <label for="discount">Discont:</label>
                <input type="number" class="form-control" name="discount" value="{{ $category->discount }}" />
            </div>
            <div class="form-group">
                <label for="exclusive">Exclusive:</label>
                <input type="number" class="form-control" name="exclusive" value="{{ $category->exclusive }}" />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection

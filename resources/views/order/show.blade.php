@extends('layouts.app')

@section('content')
    <div class="row" id="app">
        <front-page></font-page>
        <div class="col-md-12 offset-sm-2">
            <h1 class="display-3">Edit a Order</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif

                <form method="post" class="form-group" action="{{ route('order.store') }}">
                    @csrf


                    <!-- <div class="form-group">
                        <label for="order">Order:</label>
                        <input type="number" class="form-control col-md-4" name="order" value="0"/>
                    </div> -->
                        
                   <div class="row form-group">
                        <div class="col-md-4">Product</div>
                        <div class="col-md-4">Quantity</div>
                        <div class="col-md-4">Price</div>
                    </div>
                   
                    @dump($orders)

                    <div class="form-group">

                        <label for="order">Order:</label>
                        <input type="number" class="form-control col-md-4" name="order" value=""/>

                        <label for="discount"">Discount:</label>
                        <input type="number" class="form-control col-md-4" readonly name="discount" value="0"/>
                        <!-- <input type="number" hidden="true" class="input-group-text col-md-4" name="user_id" value=/> -->

                        <label for="observation">Observation:</label>
                        <input type="text" class="form-control" name="observation"/>

                        <label for="sendObservation">SendObservation:</label>
                        <input type="text" class="form-control" name="sendObservation"/>
                    </div>
                    <button type="submit" class="btn btn-primary">Add Order</button>
                </form>
            </div>
        </div>
    </div>



@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
       
    });
</script>

@endsection

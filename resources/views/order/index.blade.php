@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
        <div class="col-sm-12">
            <div class="float-rigth">
                  <a href="{{ route('order.create') }}" class="btn btn-primary float-right">New Order</a>
              </div>
        </div>      
        <div class="col-sm-12">'
            <h1 class="display-3 float-left">Orders</h1>
            <table class="table table-striped">
                <thead class="table table-dark">
                <tr>
                    <td>id</td>
                    <td>code</td>
                   <td>discount</td>
                   <td>observation</td>
{{--                    <td>City</td>--}}
{{--                    <td>Country</td>--}}
                    <td colspan = 3>Actions</td>
                </tr>
                </thead>
                <tbody>

                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->code}}</td>
                       <td>{{$order->discount}}</td>
                       <td>{{$order->observation}}</td>
{{--                        <td>{{$product->city}}</td>--}}
{{--                        <td>{{$product->country}}</td>--}}
{{--                        <td{{$product->country}}</td>--}}
                        <td>
                            <a href="{{ route('order.edit',$order->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <a href="{{ route('orderItem.show',$order->id)}}" class="btn btn-primary">Show</a>
                        </td>

                        <td>
                            <form action="{{ route('order.destroy', $order->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


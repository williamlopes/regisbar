@extends('layouts.app')

@section('content')
    <div class="row" id="app">
        <front-page></font-page>
        <div class="col-md-12 offset-sm-2">
            <h1 class="display-3">Add a Order</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif

{{--                <div class="form-group">--}}
{{--                    <select title="Produts" class="selectpicker" id="selectProducts">--}}
{{--                    <option>Select...</option>--}}
{{--                   --}}
{{--                    </select>--}}
{{--                    <td>--}}
{{--                        <td>--}}
{{--                            <label for="quantity">Quantity</label>--}}
{{--                            <input type="number" name="quantity" id="quantity">--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <a class="btn btn-primary" id="addProduct"">Add Product</a>--}}
{{--                        </td>--}}
{{--                    </td>--}}
{{--                </div>--}}

                <form method="post" class="form-group" action="{{ route('order.store') }}">
                    @csrf


                    <!-- <div class="form-group">
                        <label for="order">Order:</label>
                        <input type="number" class="form-control col-md-4" name="order" value="0"/>
                    </div> -->
                        
                    <div class="row form-group">
                        <div class="col-md-4">Product</div>
                        <div class="col-md-4">Quantity</div>
                        <div class="col-md-4">Price</div>
                    </div>
                    @foreach($products as $product)
                        <div class="input-group productItens" >
                            
                            <div class="input-group">
                                <input type="text" class="input-group-text col-md-4" value="{{$product->name}}" />
                                <input type="number" class="input-group-number col-md-4" name="quantity[{{$product->id}}]" value="0"/>
                                <div class="input-group-append col-md-2">
                                        <span class="input-group-text">R$</span>
                                        <span class="input-group-text">{{$product->price}}</span>
                                </div>
                                </div>
                                <input type="text" hidden="true" class="form-control sm-sm-3" name="product_id[{{$product->id}}]" value="{{$product->price}}"/>

                        </div>
                    @endforeach
                    <div class="form-group">

                        <label for="order">Order:</label>
                        <input type="number" class="form-control col-md-4" name="order" value="0"/>

                        <label for="discount"">Discount:</label>
                        <input type="number" class="form-control col-md-4" readonly name="discount" value="0"/>
                        <!-- <input type="number" hidden="true" class="input-group-text col-md-4" name="user_id" value=/> -->

                        <label for="observation">Observation:</label>
                        <input type="text" class="form-control" name="observation"/>

                        <label for="sendObservation">SendObservation:</label>
                        <input type="text" class="form-control" name="sendObservation"/>
                    </div>
                    <button type="submit" class="btn btn-primary">Add Order</button>
                </form>
            </div>
        </div>
    </div>



@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $("#addProduct").click(function (){
            var product = $("#selectProducts option").filter(':selected');
            var nameProduct = product.text();
            var idProduct = product.val();
            var quantity = $("#quantity").val();
            inputProduct = $("#productName").clone();
            inputProduct2 = $("#productName").clone();
            inputQuantity = $("#productQuantity").clone().removeAttr("hidden");

            $(".productItens").append(inputProduct2.val(idProduct));
            $(".productItens").append(inputProduct.removeAttr("hidden").val(nameProduct));
            $(".productItens").append(inputQuantity.val(quantity));

        });

    });
</script>

@endsection

@extends('layouts.app')

@section('content')
    <div class="row" id="app">
        <div class="col-md-12">
            <h1 class="display-3">Edit a Order {{$itens[0]->order_id}}</h1>
            
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                

                     <!-- <div class="form-group">
                        <label for="order">Order:</label>
                        <input type="number" class="form-control col-md-4" name="order" value="0"/>
                    </div> 
                          -->
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-1">Item</div>
                            <div class="col-sm-2">Product</div>
                            <div class="col-sm-2">Quantity</div>
                            <div class="col-sm-2">Discount</div>
                            <div class="col-sm-2">Price</div>
                            <div class="col-sm-1">Paid</div>                            
                            <div class="col-sm-2">Acao</div>
                        </div>
                    
                        @foreach ($itens as $item)

                        <div class="row-inline">
                            <form method="POST" class="form-inline" action="{{ route('orderItem.update', $item->id) }}">
                                @csrf
                                @method('PATCH')
                                <div class="col-sm-1">
                                    {{$item->id}}
                                </div>
                                <div class="col-sm-2">
                                    <input type="hidden" class="col" name="product_id" value="{{$item->product->id}}"/>
                                    {{$item->product->name}}
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="col" name="quantity" value="{{$item->quantity}}"/>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="col" name="discount" value="{{$item->discount}}"/>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="col" name="price" value="{{$item->price}}"/>
                                </div>
                                <div class="col-sm-1">
                                    <input type="text" class="col" name="paid" value="{{$item->paid}}"/>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-sm btn-primary">Add Order</button>
                                </div>
                            </form>
                        </div>
                                
                        
                        @endforeach
                    </div>
            </div>
        </div>
    </div>



@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        

    });
</script>

@endsection

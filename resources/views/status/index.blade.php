@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
        <div class="col-sm-12">
            <h1 class="display-3">Status</h1>
            <table class="table table-striped">
                <thead class="table table-dark">
                <tr class="">
                    <td>id</td>
                    <td>name</td>
{{--                    <td>Job Title</td>--}}
{{--                    <td>City</td>--}}
{{--                    <td>Country</td>--}}
                    <td colspan = 2>Actions</td>
                </tr>
                </thead>
                <tbody>

                @foreach($statuses as $status)
                    <tr>
                        <td>{{$status->id}}</td>
                        <td>{{$status->name}}</td>
{{--                        <td>{{$product->email}}</td>--}}
{{--                        <td>{{$product->job_title}}</td>--}}
{{--                        <td>{{$product->city}}</td>--}}
{{--                        <td>{{$product->country}}</td>--}}
{{--                        <td{{$product->country}}</td>--}}
                        <td>
                            <a href="{{ route('status.edit',$status->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form action="{{ route('status.destroy', $status->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

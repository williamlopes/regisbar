@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h4 class="display-3">Update a Status</h4>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br />
        @endif
        <form method="post" action="{{ route('status.update', $status->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value={{ $status->name }} />
            </div>

{{--            <div class="form-group">--}}
{{--                <label for="last_name">Last Name:</label>--}}
{{--                <input type="text" class="form-control" name="last_name" value={{ $status->name }} />--}}
{{--            </div>--}}

{{--            <div class="form-group">--}}
{{--                <label for="email">Email:</label>--}}
{{--                <input type="text" class="form-control" name="email" value={{ $status->code }} />--}}
{{--            </div>--}}
{{--            <div class="form-group">--}}
{{--                <label for="city">City:</label>--}}
{{--                <input type="text" class="form-control" name="city" value={{ $status->price }} />--}}
{{--            </div>--}}
{{--            <div class="form-group">--}}
{{--                <label for="country">Country:</label>--}}
{{--                <input type="text" class="form-control" name="country" value={{ $status->active }} />--}}
{{--            </div>--}}
{{--            <div class="form-group">--}}
{{--                <label for="job_title">Job Title:</label>--}}
{{--                <input type="text" class="form-control" name="job_title" value={{ $status->categories }} />--}}
{{--            </div>--}}
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection

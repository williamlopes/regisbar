@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
        <div class="col-sm-12">
            <h1 class="display-3">Products</h1>
            <table class="table table-striped">
                <thead class="table table-dark">
                <tr>
                    <td>id</td>
                    <td>code -- name</td>
                    <td>description</td>
                    <td>price</td>
                    <td>category</td>
                    <td>active</td>
                    <td colspan = 2>Actions</td>
                </tr>
                </thead>
                <tbody>

                @foreach($products as $product)
                    <tr>
{{--                        {{$product}}--}}
{{--                        'code','name','description','price', 'netWeight','weight','height','width','depth', 'exclusive', 'gtin', 'factorScore', 'isService', 'bonus' , 'active'--}}
                        <td>{{$product->id}}</td>
                        <td>{{$product->code}} -- {{$product->name}}</td>
                        <td>{{$product->description}}</td>
                        <td>R$ {{number_format( $product->price ,2,',','.') }}</td>
                        <td>{{ $product->category->first()->name ?? "" }}</td>
                        <td>
                            @if($product->active === 1)
                                Ativo
                            @else
                                Inativo
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('product.edit',$product->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form action="{{ route('product.destroy', $product->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

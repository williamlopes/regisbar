@extends('layouts.app')

@section('content')
    <div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h4 class="display-3">Update a Product</h4>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br />
        @endif

        <form method="post" action="{{ route('product.update', $product->id) }}">
            @method('PATCH')
            @csrf
{{--            <div class="form-group">--}}
{{--                <label for="first_name">ID:</label>--}}
{{--                <input type="text" class="form-control" name="id" value="{{ $product->id }}" />--}}
{{--            </div>--}}

            <div class="form-group">
                <label for="last_name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $product->name }}" />
            </div>

            <div class="form-group">
                <label for="email">Code:</label>
                <input type="number" class="form-control" name="code" value="{{ $product->code }}" />
            </div>

            <div class="form-group">
                <label for="description">Description:</label>
                <input type="text" class="form-control" name="description" value="{{ $product->description }}" />
            </div>

            <div class="form-group">
                <label for="city">Price:</label>
                <input type="number" class="form-control" name="price" value="{{ $product->price }}" />
            </div>
            <div class="form-group">
                <label for="country">Active:</label>
                <input type="text" class="form-control" name="active" value="{{ $product->active }}" />
            </div>

            <div class="form-group">
                <label for="categories">Category:</label>
                <select class="form-control" name="categories">
                    <option name="categories" value=""></option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}"
                            @if( isset($product->category[0]) and $category->id === $product->category[0]->id)
                                selected
                            @endif
                        >
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
            </div>
{{--            <div class="form-group">--}}
{{--                <label for="job_title">Job Title:</label>--}}
{{--                <input type="text" class="form-control" name="categories" value="{{ $product->categories }}" />--}}
{{--            </div>--}}
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
